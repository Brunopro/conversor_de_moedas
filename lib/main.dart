import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

import 'dart:async';
import 'dart:convert';
import 'package:http/http.dart' as http;

//import 'package:mingup/screen/login_screen.dart';

const request =
    'https://api.hgbrasil.com/finance?format=json-cors&key=19ad6af2';

double resdolar;
double reseuro;

dynamic hasError = false;

void main() async {
  runApp(MaterialApp(
    home: SplashScreen(),
    theme: ThemeData(hintColor: Colors.amber, primaryColor: Colors.white),
  ));
}

Future<Map> getData() async {
  http.Response response = await http.get(request);
  return json.decode(response.body);
}

class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  final realController = TextEditingController();
  final dolarController = TextEditingController();
  final euroController = TextEditingController();

  double dolar;
  double euro;

  //Funções para quando fizermos alguma mudança nos campos
  void _realChanged(String text) {
    //se campo texto for vazio
    if (text.isEmpty) {
      _clearAll();
      return;
    }

    double real = double.parse(text);
    dolarController.text = (real / dolar).toStringAsFixed(2);
    euroController.text = (real / euro).toStringAsFixed(2);
  }

  void _dolarChanged(String text) {
    //se campo texto for vazio
    if (text.isEmpty) {
      _clearAll();
      return;
    }

    double dolar = double.parse(text);
    realController.text = (dolar * this.dolar).toStringAsFixed(2);
    euroController.text = (dolar * this.dolar / euro).toStringAsFixed(2);
  }

  void _euroChanged(String text) {
    //se campo texto for vazio
    if (text.isEmpty) {
      _clearAll();
      return;
    }

    double euro = double.parse(text);
    realController.text = (euro * this.euro).toStringAsFixed(2);
    dolarController.text = (euro * this.euro / dolar).toStringAsFixed(2);
  }

  void _clearAll() {
    realController.text = "";
    dolarController.text = "";
    euroController.text = "";
  }

  @override
  Widget build(BuildContext context) {
    if (hasError == false) {
      euro = reseuro;
      dolar = resdolar;
      return Scaffold(
          backgroundColor: Colors.black,
          appBar: AppBar(
            title: Text("\$ Conversor \$"),
            backgroundColor: Colors.amber,
            centerTitle: true,
          ),
          body: SingleChildScrollView(
              padding: EdgeInsets.all(15),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: <Widget>[
                  Icon(Icons.monetization_on, size: 150.0, color: Colors.amber),
                  buildTextField("Reais", "R\$", realController, _realChanged),
                  Divider(),
                  buildTextField(
                      "Dolares", "US\$", dolarController, _dolarChanged),
                  Divider(),
                  buildTextField("Euros", "€", euroController, _euroChanged),
                ],
              )));
    } else {
      return Scaffold(
          backgroundColor: Colors.black,
          appBar: AppBar(
            title: Text("\$ Conversor \$"),
            backgroundColor: Colors.amber,
            centerTitle: true,
          ),
          body: Center(
            child: Text('Error: $hasError',
                style: TextStyle(backgroundColor: Colors.white)),
          ));
    }
  }
}

Widget buildTextField(String label, String prefix,
    TextEditingController controller, Function function) {
  return TextField(
    controller: controller,
    decoration: InputDecoration(
      labelText: label,
      labelStyle: TextStyle(color: Colors.amber, fontSize: 25.0),
      border: OutlineInputBorder(),
      prefixText: prefix + " ",
    ),
    style: TextStyle(color: Colors.amber),
    onChanged: function,
    keyboardType: TextInputType.numberWithOptions(decimal: true),
  );
}

// This is the Splash Screen
class SplashScreen extends StatefulWidget {
  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen>
    with SingleTickerProviderStateMixin {
  AnimationController _animationController;
  Animation<double> _animation;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    _animationController = new AnimationController(
        vsync: this, duration: new Duration(milliseconds: 2000));
    _animation = new CurvedAnimation(
      parent: _animationController,
      curve: Curves.easeOut,
    );

    _animation.addListener(() => this.setState(() {}));
    _animationController.forward();

    /* Caso não tenha nenhuma função assioncrona para carregar utilize esse código
    Timer(Duration(seconds: 20), () {
      // O que fazer dps que o tempo da s-plashscreen terminar ? => Navigator.push(context, MaterialPageRoute(builder: (context) => LoginScreen()));
    });
    */

    getData()
        .then((data) => {
              print("data do getData $data"),
              resdolar = data["results"]["currencies"]["USD"]["buy"],
              reseuro = data["results"]["currencies"]["EUR"]["buy"],

              //Fazer a splashscren ter maior duração
              Future.delayed(Duration(seconds: 4)).then((_) {
                Navigator.of(context).pushReplacement(
                    MaterialPageRoute(builder: (context) => Home())
                );
              })
            })
        .catchError((onError) => {
              hasError = onError,
              Future.delayed(Duration(seconds: 4)).then((_) {
                Navigator.of(context).pushReplacement(
                    MaterialPageRoute(builder: (context) => Home()));
              })
            });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        fit: StackFit.expand,
        children: <Widget>[
          Container(
              child: Image.asset(
            'images/splashscreenbg.png',
            fit: BoxFit.cover,
            height: 1000.0,
          )),
          Column(mainAxisAlignment: MainAxisAlignment.start, children: <Widget>[
            Expanded(
              flex: 2,
              child: Container(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    new Image(
                      image: new AssetImage("images/icons/moeda.png"),
                      width: _animation.value * 150,
                      
                    ),
                    Padding(padding: EdgeInsets.only(top: 10.0)),
                    Text(
                      "Conversor facil",
                      style: TextStyle(
                          color: Colors.black87,
                          fontSize: 30.0,
                          fontWeight: FontWeight.bold),
                    )
                  ],
                ),
              ),
            ),
          ])
        ],
      ),
    );
  }
}
